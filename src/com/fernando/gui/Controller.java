package com.fernando.gui;

import com.fernando.base.Equipo;
import javafx.scene.control.TableSelectionModel;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelListener;
import java.util.ArrayList;

/**
 * Created by DAM on 11/11/2016.
 */
public class Controller implements ListSelectionListener{
    Ventana view;
    Model model;
    public Controller(Ventana view, Model model){
        this.model = model;
        this.view = view;
        poblarTablaEquipos();
        inicializarTabla();
    }
    public void inicializarTabla(){
        view.tbEquipos.setSelectionModel(ListSelectionModel.SINGLE_SELECTION);
    }

    public void poblarTablaEquipos(){
        String[] datos = null;
        ArrayList<Equipo> equipos = model.obtenerEquipos();
        view.dtmEquipos.setNumRows(0);
        for(Equipo equipo:equipos){
            datos=new String[]{
                equipo.getNombre(),
                equipo.getPatrocinador(),
                equipo.getColor1(),
                equipo.getColor2(),
                equipo.getCategoria()
            };
            view.dtmEquipos.addRow(datos);
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent listSelectionEvent) {
        String a = (String) view.tbEquipos.getValueAt(view.tbEquipos.getSelectedRow(),0);
        Equipo equipo = model.obtenerEquipos(a);
    }
}
