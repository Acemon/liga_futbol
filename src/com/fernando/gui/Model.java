package com.fernando.gui;

import com.fernando.base.Equipo;
import com.mysql.jdbc.Connection;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Created by DAM on 11/11/2016.
 */
public class Model {
    private Connection conexion;

    public Model(){

    }

    /**
     * Conexion a la base de datos (Con fichero props)
     */
    private void conectar() throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream("config_bbdd.props"));
        String host=properties.getProperty("host");
        String database=properties.getProperty("database");
        String user=properties.getProperty("username");
        String pass = properties.getProperty("password");

//        Class.forName("com.mysql.jdbc.Driver").newInstance();
//        conexion = DriverManager.getConnection(
//                "jdbc:mysql://localhost:3306/basededatos",
//                "usuario", "contraseña");
    }

    /**
     * Desconexion de la base de datos
     */
    private void desconectar(){

    }

    /**
     * Da de alta a un equipo
     */
    public void nuevoEquipo(Equipo equipo){

    }

    /**
     * Modifica un equipo existente
     * @param nombre nombre del equipo antiguo
     * @param equipo Equipo a rellenar
     */
    public void modificarEquipo (String nombre, Equipo equipo){

    }

    /**
     * Elimina un equipo existente
     * @param nombre
     */
    public void eliminarEquipo (String nombre){

    }

    /**
     * Obtiene n equipo
     * @return devuelve a todos los equipos
     */
    public ArrayList<Equipo> obtenerEquipos (){
        return null;
    }

    /**
     * Obtiene los equipos dados por un nombre
     * @param busqueda nombre del equipo a buscar
     * @return equipos devueltos cumpliendo el orden de busqueda
     */
    public ArrayList<Equipo> obtenerEquipos (String busqueda){
        return null;
    }
}
