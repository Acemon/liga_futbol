package com.fernando.gui;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Created by DAM on 11/11/2016.
 */
public class Ventana {
    JPanel panel1;
    JRadioButton rbMysql;
    JRadioButton rbPostgre;
    JTextField etUsuario;
    JButton btConectar;
    JPasswordField etContraseña;
    JButton btNuevo;
    JButton btGuardar;
    JButton btCancelar;
    JButton btModificar;
    JTabbedPane tabbedPane1;
    JTextField tfNombre;
    JTextField tfPatrocinador;
    JTextField tfColor1;
    JTextField tfColor2;
    JButton btEliminar;
    JTextField textField1;
    JTextField textField2;
    JTextField textField3;
    JTextField textField4;
    JTextField textField5;
    JComboBox cbCategoria;
    JTable tbEquipos;
    JLabel lEstado;
    ButtonGroup grupo;
    DefaultTableModel dtmEquipos;

    public Ventana() {
        JFrame frame = new JFrame("Ventana");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        dtmEquipos = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int i, int i1) {
                return false;
            }
        };

        dtmEquipos.addColumn("Nombre");
        dtmEquipos.addColumn("Patrocinador");
        dtmEquipos.addColumn("Color1");
        dtmEquipos.addColumn("Color2");
        dtmEquipos.addColumn("Categoria");
        tbEquipos.setModel(dtmEquipos);

        grupo = new ButtonGroup();
        grupo.add(rbMysql);
        grupo.add(rbPostgre);
    }
}
