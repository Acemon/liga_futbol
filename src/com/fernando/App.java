package com.fernando;

import com.fernando.gui.Controller;
import com.fernando.gui.Model;
import com.fernando.gui.Ventana;

/**
 * Created by DAM on 11/11/2016.
 */
public class App {
    public static void main(String args[]){
        Ventana view = new Ventana();
        Model model = new Model();
        Controller controller = new Controller(view, model);
    }
}
